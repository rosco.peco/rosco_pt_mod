# ProTracker MOD format utils

Just a (minimally) tidied-up version of some code I was working on
for reading ProTracker MOD files.

Just prints out a bunch of information to the screen. Expects mod data
to be linked in via a header file for the time being.

Can build for local or rosco environments.

## Building

### For rosco_m68k

ROSCO_M68K_DIR=/path/to/rosco_m68k make clean all

### For local

make -f native.mk clean all


#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <basicio.h>
#include <xosera_m68k_api.h>
#include <xosera_m68k_defs.h>
#include "pt_mod.h"
#include "dprintf.h"

#define SILENCE_VADDR 0xffff

#define BUFFER_A 0xa000
#define BUFFER_B 0xa400

#ifdef NTSC
#define TENTH_MS_PER_FRAME 166
#else 
#define TENTH_MS_PER_FRAME 200
#endif

#define DEFAULT_SPEED 6
//#define DEBUG

//extern void install_intr();
//extern void remove_intr();

/*extern volatile*/ uint16_t StepFrames;

typedef struct {
    PtMemorySample  *current_sample;
    uint16_t        next_chunk_start;
    uint16_t        next_buffer_start;
    uint16_t        period;
    uint16_t        buffer_size;
    uint16_t        buffer_a_addr;
    uint16_t        buffer_b_addr;
} Channel;

/* Returns true if this we loaded the last chunk */
static bool load_sample_chunk(
        PtMemorySample *sample, 
        uint16_t addr, 
        uint16_t chunk_start, 
        uint16_t chunk_len,
        uint16_t *outActual
) {
    xm_setw(XR_ADDR, addr);

    uint16_t chunk_end = chunk_start + chunk_len;
    if (chunk_end > sample->length) {
        chunk_end = sample->length;
    }

#ifdef DEBUG
    dprintf("LOAD: %d-%d in buffer at 0x%04x\n", chunk_start, chunk_end, addr);
#endif

    *outActual = chunk_end-chunk_start;

    for (uint16_t i = chunk_start; i < chunk_end; i++) {
        xm_setw(XR_DATA, sample->data[i]);
    }

    return chunk_end == sample->length;
}

/* Returns true if this we loaded the last chunk; Mutates state in argument and Xosera */
static bool load_next_chunk(Channel *channel, uint16_t *outActual) {
    bool result = load_sample_chunk(
            channel->current_sample, 
            channel->next_buffer_start, 
            channel->next_chunk_start, 
            channel->buffer_size,
            outActual
    );

    channel->next_buffer_start = channel->next_buffer_start == channel->buffer_b_addr 
        ? channel->buffer_a_addr 
        : channel->buffer_b_addr;

    channel->next_chunk_start = channel->next_chunk_start + channel->buffer_size;

    return result;
}

static void init_channel(
        Channel *channel, 
        PtMemorySample *sample,
        uint16_t period,
        uint16_t buffer_a,
        uint16_t buffer_b,
        uint16_t buffer_size
) {
    channel->current_sample = sample;
    channel->period = period;
    channel->next_chunk_start = 0;
    channel->next_buffer_start = buffer_a;
    channel->buffer_a_addr = buffer_a;
    channel->buffer_b_addr = buffer_b;
    channel->buffer_size = buffer_size;
}

static PtMemorySample silence;

static void stop_channel(Channel *channel) {
#ifdef debug
    dprintf("Stop channel\n");
#endif

    // TODO loop pos etc
    channel->current_sample = &silence;
}

static inline uint16_t makeStereoVolume(uint8_t volume) {
    return volume << 8 | volume;
}

static void xosera_channel_ready(Channel *channel, bool force) {
    if (channel->current_sample->length == 0) {
        xreg_setw(VID_CTRL, 0x0000);
        xreg_setw(AUD0_START, 0xffff);
        xreg_setw(AUD0_LENGTH, 0);
    } else {
        uint16_t actualLoaded = 0;
        bool last = load_next_chunk(channel, &actualLoaded);

        xreg_setw(VID_CTRL, 0x0010);
        xreg_setw(AUD0_PERIOD, force 
                ? channel->period | 0x8000
                : channel->period);

        if (force) {
            // Only set volume if force (meaning this is a new note)
            // to avoid stepping on any current Cxx command...
            xreg_setw(AUD0_VOL, makeStereoVolume(channel->current_sample->sample->volume));
        }

        xreg_setw(AUD0_START, channel->next_buffer_start == channel->buffer_b_addr 
            ? channel->buffer_a_addr 
            : channel->buffer_b_addr
        );

        xreg_setw(AUD0_LENGTH, actualLoaded | 0x8000);

        if (last) {
            stop_channel(channel);
        }
    }
}

static volatile int nextPatternPos = 0;
static volatile bool stepped = false;
static int patternPos = 0;
static int position = 0;
static int pattern = 0;
uint16_t tickTenMs = DEFAULT_SPEED * TENTH_MS_PER_FRAME;
uint16_t timerEnd = 0;

static uint16_t calculateNextTimerEnd() {
    uint16_t newTimerEnd = xm_getw(TIMER) + tickTenMs;

    if (newTimerEnd < timerEnd) {
        // HACK - timer rolled over, just wait for it to zero...
        //        causes slight glitches occasionally but will go
        //        away later...
        while (xm_getw(TIMER) > newTimerEnd);
        newTimerEnd = xm_getw(TIMER) + tickTenMs;
    }

    return newTimerEnd;
}

static inline void handleEffect(uint16_t effect, bool thisChannel) {
    if ((effect & 0xF00) == 0xF00) {
        // speed - recalculate timer so this takes effect immediately
        StepFrames = effect & 0x00FF;
        tickTenMs = StepFrames * TENTH_MS_PER_FRAME;
        timerEnd = calculateNextTimerEnd();
    }

    if (thisChannel && ((effect & 0xF00) == 0xC00)) {
        // Volume for our current channel - do it
        xreg_setw(AUD0_VOL, makeStereoVolume(effect & 0x00FF));
    }
}

/* **Not** Called by interrupt handler, but on timer */
void modTimeStep() {
    nextPatternPos = patternPos + 1;
    stepped = true;
}

int xosera_play(PtMod *mod) {
    while (checkchar()) {
        readchar();
    }

    PtMemorySample samples[31];
    PtPopulateMemorySamples(mod, samples);
    PtPattern *patterns = PtPatternData(mod);

    Channel channel;

    // polling for precise timing, no interrupt needed atm
//    install_intr();
    
    dprintf("Samples populated in memory\n");

    // Prime the pump
    const int mod_channel = 0;
    pattern = mod->positions[0];
    stepped = true;
    StepFrames = 6;
    timerEnd = xm_getw(TIMER) + tickTenMs;

    while (!checkchar()) {
        if (xm_getw(TIMER) >= timerEnd) {
            modTimeStep();
            timerEnd = calculateNextTimerEnd();
        }

        /* race */
        if (stepped) {
            stepped = false;
            patternPos = nextPatternPos;
            /* condition (when back to using interrupts) */

            if (patternPos == 64) {
                if (++position >= mod->song_length) {
                    break;
                } else {
                    pattern = mod->positions[position];
                    patternPos = 0;
                }
            }

            PtPatternRow row = patterns[pattern].rows[patternPos];

            int sampleNumber = PtSampleNumber(row.channel_notes[mod_channel]);
            int period = PtNotePeriod(row.channel_notes[mod_channel]);
            int xoPeriod = PtXoseraPeriod(period);

            // HACK - We need to take commands from all tracks, not just the one
            // we're playing. Won't be an issue when we have four channels ;D 
            //
            // This is because we want speed (and other global) commands  to work 
            // even if they're not on track one...
            uint16_t effect1 = PtEffect(row.channel_notes[0]);
            uint16_t effect2 = PtEffect(row.channel_notes[1]);
            uint16_t effect3 = PtEffect(row.channel_notes[2]);
            uint16_t effect4 = PtEffect(row.channel_notes[3]);

#ifdef DEBUG
            dprintf("%02x[%02x]: SMPL: %02x; PD: %04d [%3s] (Xosera: %05d) CMD: %03x:%03x:%03x:%03x (SPD: %d / %d ms/10)\n", 
                    pattern, patternPos, sampleNumber, period, PtNoteName(period), xoPeriod, 
                    effect1, effect2, effect3, effect4, StepFrames, tickTenMs);
#endif

            if (sampleNumber > 0) {
                init_channel(&channel, &samples[sampleNumber - 1], xoPeriod, BUFFER_A, BUFFER_B, 0x400);
                xosera_channel_ready(&channel, true);
            }

            // Handle these *after* loading so volume can take effect immediately
            handleEffect(effect1, true);
            handleEffect(effect2, false);
            handleEffect(effect3, false);
            handleEffect(effect4, false);             

            // Service sample streaming
            if ((xreg_getw(VID_CTRL) & 0x0010) == 0) {
                xosera_channel_ready(&channel, false);
            }
        }
    }

//    remove_intr();

    xreg_setw(VID_CTRL, 0x0000);
    xosera_init(0);

    dprintf("Either done or you pressed a key; Bailing...\n");
    return 1;
}


